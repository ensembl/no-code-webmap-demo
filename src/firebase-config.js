// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore, connectFirestoreEmulator } from "firebase/firestore";
import { getAuth, connectAuthEmulator } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD9jujoq31S3Ez37iSS1rjQgQD07x9DnZY",
  authDomain: "no-code-webmap-demo.firebaseapp.com",
  projectId: "no-code-webmap-demo",
  storageBucket: "no-code-webmap-demo.appspot.com",
  messagingSenderId: "918201216901",
  appId: "1:918201216901:web:d22fdd91ea0036017b9eec",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app);
const auth = getAuth(app);

if (window.location.hostname === "localhost") { //eslint-disable-line
  connectFirestoreEmulator(db, "localhost", 8080);
  // functions.useEmulator("localhost", 5001);
  connectAuthEmulator(auth, "http://localhost:9099");
  console.warn("You are running in the emulator environment."); // eslint-disable-line no-console
}

export {
  db, auth,
};
