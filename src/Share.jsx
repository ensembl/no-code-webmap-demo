import React, { useState } from "react";

import PropTypes from "prop-types";

import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import LinkIcon from "@mui/icons-material/Link";
import Snackbar from "@mui/material/Snackbar";

import VisibilityIcon from "@mui/icons-material/Visibility";

function Share({ rootUrl, mapId }) {
  const [copy, setCopy] = useState("ready");

  const onCopyLink = () => {
    navigator.clipboard.writeText(`${rootUrl}/maps/${mapId}`); //eslint-disable-line
    setCopy("copied");
  };

  const onPreview = () => {
    window.open(`${rootUrl}/maps/${mapId}`, '_blank') //eslint-disable-line
  };

  return (
    <Box>
      <Box>
        <h3 style={{ display: "inline" }}>Step 3:</h3>
        <p style={{ marginLeft: "5px", display: "inline-block" }}>Copy the link or embed it in your site.</p>
      </Box>
      <Box>
        <List style={{ display: "flex", flexDirection: "row" }} dense>
          <ListItem onClick={onPreview} button id="copy">
            <ListItemIcon>
              <VisibilityIcon />
            </ListItemIcon>
            <ListItemText primary="Preview" />
          </ListItem>
          <ListItem onClick={onCopyLink} button id="copy">
            <ListItemIcon>
              <LinkIcon />
            </ListItemIcon>
            <ListItemText primary="Copy link" />
          </ListItem>
        </List>
      </Box>
      <Snackbar
        open={copy === "copied"}
        autoHideDuration={2000}
        onClose={() => { setCopy("ready"); }}
      >
        <Alert severity="success">Link copied!</Alert>
      </Snackbar>
    </Box>
  );
}

Share.propTypes = {
  rootUrl: PropTypes.string.isRequired,
  mapId: PropTypes.string.isRequired,
};

export default Share;
