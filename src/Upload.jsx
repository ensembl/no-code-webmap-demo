import React, { useState } from "react";

import PropTypes from "prop-types";
import Papa from "papaparse";

import { styled } from "@mui/material/styles";

import Alert from "@mui/material/Alert";
import Button from "@mui/material/Button";
import Chip from "@mui/material/Chip";
import Input from "@mui/material/Input";
import Box from "@mui/material/Box";
import Snackbar from "@mui/material/Snackbar";
import Stack from "@mui/material/Stack";
import Tooltip, { tooltipClasses } from "@mui/material/Tooltip";
import HelpRoundedIcon from "@mui/icons-material/HelpRounded";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";

import "./Upload.css";

function Upload({
  csvLocations, setCsvLocations, setShowMap, csvFileName, setCsvFileName,
}) {
  const [upload, setUpload] = useState("empty");

  const onFileReaderLoad = (e) => {
    setShowMap(false);
    const res = Papa.parse(e.target.result);
    setCsvLocations(res.data.slice(1, res.data.length)
      .map((v) => ({ location: v[0], name: v[1], description: v[2] })));
    setUpload("uploaded");
  };

  const onCsvUpload = (e) => {
    const file = e.target.files[0];
    setCsvFileName(file.name);
    const fr = new global.FileReader();
    fr.onload = onFileReaderLoad;
    fr.readAsText(file);

    e.target.value = null;
  };

  const NoMaxWidthTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} classes={{ popper: className }} /> // eslint-disable-line
  ))({
    [`& .${tooltipClasses.tooltip}`]: {
      maxWidth: "none",
    },
  });

  function createData(location, name, description) {
    return {
      location, name, description,
    };
  }

  const rows = [
    createData("Chicago", "City of Chicago", "It is the most populous city in the U.S. state of Illinois"),
    createData("Seattle", "City of Seattle", "It is the largest city in the state of Washington"),
  ];

  const onDelete = () => {
    setCsvLocations([]);
    setShowMap(false);
  };

  const tooltip = (
    <Box>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>locations</TableCell>
              <TableCell align="left">name</TableCell>
              <TableCell align="left">description</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.location}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row.location}
                </TableCell>
                <TableCell align="left">{row.name}</TableCell>
                <TableCell align="left">{row.description}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );

  return (
    <>
      <Stack direction="column" alignItems="center">
        <Stack direction="row" alignItems="center">
          <label htmlFor="contained-button-file">
            <Input
              onChange={onCsvUpload}
              style={{ display: "none" }}
              accept=".csv"
              id="contained-button-file"
              multiple
              type="file"
            />
            <Button style={{ color: "#5B85F2" }} component="span">
              Import CSV
            </Button>
          </label>
          <NoMaxWidthTooltip
            placement="bottom"
            title={tooltip}
            componentsProps={{
              tooltip: {
                sx: {
                  fontWeight: "normal",
                  padding: "10px",
                },
              },
            }}
          >
            <HelpRoundedIcon color="primary" style={{ height: "1rem", color: "#5B85F2" }} />
          </NoMaxWidthTooltip>
        </Stack>
        {csvLocations.length !== 0
          && (
            <Box marginTop="5px">
              <Chip
                variant="outlined"
                onDelete={onDelete}
                label={`${csvLocations.length} rows from ${csvFileName}`}
              />
            </Box>
          )}
      </Stack>
      <Snackbar
        open={upload === "uploaded"}
        autoHideDuration={3000}
        onClose={() => setUpload("ready")}
      >
        <Alert severity="success">Uploaded successfully</Alert>
      </Snackbar>

    </>
  );
}

Upload.propTypes = {
  csvLocations: PropTypes.arrayOf(PropTypes.any).isRequired, //eslint-disable-line
  setCsvLocations: PropTypes.func.isRequired,
  setShowMap: PropTypes.func.isRequired,
  csvFileName: PropTypes.string.isRequired,
  setCsvFileName: PropTypes.func.isRequired,
};

export default Upload;
