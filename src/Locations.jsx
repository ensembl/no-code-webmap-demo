import React, { useState } from "react";

import PropTypes from "prop-types";

import Input from "@mui/material/Input";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";

import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";

import StyledTooltip from "./UI/StyledTooltip";

function Locations({ locations, setLocations, setShowMap }) {
  const [index, setIndex] = useState(1);

  const onAddLocationClicked = () => {
    setIndex(index + 1);
    setShowMap(false);
  };

  const onDeleteLocationClicked = () => {
    setIndex(index - 1);
    const keys = Object.keys(locations);
    const lastKey = keys[keys.length - 1];

    const newLocations = locations;
    delete newLocations[lastKey];
    setLocations(newLocations);
    setShowMap(false);
  };

  const onValueChanged = (type, value, i) => {
    setShowMap(false);
    if (type === "location") {
      setLocations({
        ...locations,
        [i]: {
          ...locations[i],
          location: value,
        },
      });
    }

    if (type === "name") {
      setLocations({
        ...locations,
        [i]: {
          ...locations[i],
          name: value,
        },
      });
    }

    if (type === "description") {
      setLocations({
        ...locations,
        [i]: {
          ...locations[i],
          description: value,
        },
      });
    }
  };

  return (
    <>
      <Box>
        <h3 style={{ display: "inline" }}>Step 1:</h3>
        <p style={{ marginLeft: "5px", display: "inline-block" }}>Add locations or import from a CSV file.</p>
      </Box>
      <Stack>
        {[...Array(index)].map((value, i) => (
          <Stack
            key={i} //eslint-disable-line
            index={i}
            marginBottom="20px"
            direction="row"
            alignItems="flex-end"
          >
            <Stack direction="column" alignItems="flex-start" marginRight="12px">
              <Stack direction="row" alignItems="center">
                <InputLabel>Location</InputLabel>
                <StyledTooltip title="addresses, city, country" />
              </Stack>
              <Input
                onChange={(e) => { onValueChanged("location", e.target.value, i); }}
                style={{ width: "500px" }}
                fullWidth
                placeholder="Chicago / 121 N LaSalle St, Chicago, IL 60602"
              />
            </Stack>
            <Stack direction="column" alignItems="flex-start" marginRight="12px">
              <Stack direction="row" alignItems="center">
                <InputLabel>Name</InputLabel>
                <StyledTooltip title="Name will be used for the title of the popup" />
              </Stack>
              <Input
                onChange={(e) => { onValueChanged("name", e.target.value, i); }}
                placeholder="Ensembl"
              />
            </Stack>
            <Stack direction="column" alignItems="flex-start">
              <Stack direction="row" alignItems="center">
                <InputLabel>Description</InputLabel>
                <StyledTooltip title="Description will be used for the body of the popup" />
              </Stack>
              <Input
                style={{ width: "500px" }}
                onChange={(e) => { onValueChanged("description", e.target.value, i); }}
                placeholder="Ensembl will simplify your mapping needs!"
                multiline
              />
            </Stack>
          </Stack>
        ))}
      </Stack>
      <Stack marginTop="20px" direction="row">
        <Box marginRight="20px">
          <Button style={{ color: "#5B85F2" }} onClick={onAddLocationClicked}>
            <Stack direction="row" alignItems="center">
              <AddIcon style={{ height: "1rem" }} />
              Location
            </Stack>
          </Button>
        </Box>
        <Box>
          <Button style={{ color: "#5B85F2" }} onClick={onDeleteLocationClicked}>
            <Stack direction="row" alignItems="center">
              <RemoveIcon style={{ height: "1rem" }} />
              Location
            </Stack>
          </Button>
        </Box>
      </Stack>
    </>
  );
}

Locations.propTypes = {
  locations: PropTypes.objectOf(PropTypes.any).isRequired, //eslint-disable-line
  setLocations: PropTypes.func.isRequired,
  setShowMap: PropTypes.func.isRequired,
};

export default Locations;
