import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

import React, { useState, useRef } from "react";

import Locations from "./Locations";
import MapContainer from "./MapContainer";
import Upload from "./Upload";
import Embed from "./Embed";
import Share from "./Share";
import AppToolbar from "./AppToolbar";
import Feedback from "./Feedback";

import "./Home.css";

function Home() {
  const [locations, setLocations] = useState({});
  const [csvLocations, setCsvLocations] = useState([]);
  const [showMap, setShowMap] = useState(false);
  const [mapId, setMapId] = useState("");

  const csvFileName = useRef("");

  const setCsvFileName = (name) => {
    csvFileName.current = name;
  };

  const rootUrl = window.location.hostname === "localhost" //eslint-disable-line
    ? "http://localhost:3000" : "https://app.ensembl.ai";

  return (
    <Box>
      <AppToolbar />
      <Box
        className="App"
        display="flex"
        alignItems="center"
        justifyContent="center"
        flexDirection="column"
        marginTop="20px"
      >
        <Locations locations={locations} setLocations={setLocations} setShowMap={setShowMap} />
        <Box margin="20px 0">
          <Upload
            csvLocations={csvLocations}
            setCsvLocations={setCsvLocations}
            setShowMap={setShowMap}
            csvFileName={csvFileName.current}
            setCsvFileName={setCsvFileName}
          />
        </Box>
        <Box marginTop="50px" marginBottom="20px">
          <Button
            style={{ backgroundColor: "#5B85F2" }}
            variant="contained"
            onClick={() => { setShowMap(true); }}
          >
            Create map
          </Button>
        </Box>
        {showMap && (
          <MapContainer
            mapId={mapId}
            setMapId={setMapId}
            locations={[...Object.values(locations), ...csvLocations]}
          />
        )}
        {showMap && (
          <Box margin="30px">
            <Share rootUrl={rootUrl} mapId={mapId} />
          </Box>
        )}
        {showMap
          && (
            <Box>
              <Embed rootUrl={rootUrl} mapId={mapId} />
            </Box>
          )}

        {showMap && (
          <Box marginBottom="100px" marginTop="30px">
            <Feedback />
          </Box>
        )}
      </Box>
    </Box>
  );
}

export default Home;
