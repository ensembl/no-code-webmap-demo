import React from "react";

import Box from "@mui/material/Box";

import AppBar from "@mui/material/AppBar";
import Avatar from "@mui/material/Avatar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

import Logo from "./images/logo.png";

function AppToolbar() {
  return (
    <AppBar
      style={{
        display: "flex",
        justifyContent: "center",
      }}
      color="transparent"
      position="static"
    >
      <Toolbar>
        <Avatar alt="Ensembl" src={Logo} />
        <Typography variant="h6" component="div">
          <Box fontWeight={600}>Ensembl</Box>
        </Typography>
        <Typography marginLeft="10px" variant="p" component="div">
          mapping simplify
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

export default AppToolbar;
