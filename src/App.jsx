import React from "react";
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { ThemeProvider, createTheme } from "@mui/material/styles";

import Home from "./Home";
import Maps from "./Maps";

const theme = createTheme({
  typography: {
    fontFamily: [
      "Poppins",
      "Roboto",
      "\"Helvetica Neue\"",
      "Arial",
      "sans-serif",
    ].join(","),
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/maps/:id" element={<Maps />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>

  );
}

export default App;
