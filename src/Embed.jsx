import React, { useState } from "react";

import PropTypes from "prop-types";

import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import Snackbar from "@mui/material/Snackbar";

function Embed({ rootUrl, mapId }) {
  const [copy, setCopy] = useState("ready");

  const iFrameValue = `<iframe id="map-${mapId}" aria-label="Map" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="400" width="600" src=${rootUrl}/maps/${mapId}></iframe>`;

  const onCopy = () => {
    navigator.clipboard.writeText(iFrameValue); //eslint-disable-line
    setCopy("copied");
    setTimeout(() => {
      setCopy("ready");
    }, 2000);
  };

  return (
    <Box margin="20px 0 40px 0">
      <div style={{ marginBottom: "20px" }}><b>Embed in your site 💻</b></div>
      <TextField
        value={iFrameValue}
        label="Code snippet"
        style={{ width: "300px" }}
        size="small"
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton onClick={onCopy} edge="end" color="primary">
                <ContentCopyIcon />
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      <Snackbar
        open={copy === "copied"}
        autoHideDuration={7000}
      >
        <Alert severity="success">Code snippet copied!</Alert>
      </Snackbar>
    </Box>
  );
}

Embed.propTypes = {
  rootUrl: PropTypes.string.isRequired,
  mapId: PropTypes.string.isRequired,
};

export default Embed;
