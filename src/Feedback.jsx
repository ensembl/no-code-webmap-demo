import React, { useState } from "react";

import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import Stack from "@mui/material/Stack";
import Snackbar from "@mui/material/Snackbar";

import TextField from "@mui/material/TextField";

import {
  collection, addDoc, serverTimestamp,
} from "firebase/firestore";
import { db } from "./firebase-config";

import Step from "./UI/Step";

function Feedback() {
  const [open, setOpen] = useState(false);
  const [feedback, setFeedback] = useState("");
  const [email, setEmail] = useState("");
  const [feedbackStatus, setFeedbackStatus] = useState("ready");

  const onClose = () => {
    setOpen(false);
  };

  const onSubmit = async () => {
    await addDoc(
      collection(
        db,
        "feedbacks",
      ),
      {
        createdAt: serverTimestamp(),
        feedback,
        email,
      },
    );
    setOpen(false);
    setFeedbackStatus("sent");
  };

  return (
    <>
      <Stack>
        <Step stepNumber="4" text="Let us know how to improve!" />
        <Button onClick={() => setOpen(true)} style={{ fontSize: "1.5rem" }}>📝</Button>
      </Stack>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>Feedback 😀</DialogTitle>
        <DialogContent>
          <Box marginBottom="30px">
            <DialogContentText>
              Bugs, feedbacks, additional features?
            </DialogContentText>
            <TextField
              style={{ width: "500px" }}
              autoFocus
              margin="dense"
              id="name"
              fullWidth
              variant="standard"
              onChange={(e) => { setFeedback(e.target.value); }}
            />
          </Box>
          <Box>
            <DialogContentText>
              How can we reach out to you?
            </DialogContentText>
            <TextField
              margin="dense"
              id="name"
              label="email address"
              type="email"
              fullWidth
              variant="standard"
              onChange={(e) => { setEmail(e.target.value); }}
            />
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={onSubmit}>Submit</Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        open={feedbackStatus === "sent"}
        autoHideDuration={3000}
        onClose={() => { setFeedbackStatus("ready"); }}
      >
        <Alert severity="success">Feedback sent!</Alert>
      </Snackbar>
    </>
  );
}

export default Feedback;
