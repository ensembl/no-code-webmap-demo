import React from "react";

import PropTypes from "prop-types";

import HelpRoundedIcon from "@mui/icons-material/HelpRounded";

import Tooltip from "@mui/material/Tooltip";

function StyledTooltip({ title, placement }) {
  return (
    <Tooltip title={title} placement={placement}>
      <HelpRoundedIcon color="primary" style={{ height: "1rem", color: "#5B85F2" }} />
    </Tooltip>

  );
}

StyledTooltip.propTypes = {
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]).isRequired,
  placement: PropTypes.string,
};

StyledTooltip.defaultProps = {
  placement: "right",
};

export default StyledTooltip;
