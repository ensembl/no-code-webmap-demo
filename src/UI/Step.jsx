import React from "react";

import PropTypes from "prop-types";

import Box from "@mui/material/Box";

function Step({ stepNumber, text }) {
  return (
    <Box>
      <h3 style={{ display: "inline" }}>
        {`Step ${stepNumber}:`}
      </h3>
      <p style={{ marginLeft: "5px", display: "inline-block" }}>{`${text}`}</p>
    </Box>
  );
}

Step.propTypes = {
  stepNumber: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Step;
