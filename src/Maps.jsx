import React, { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import mapboxgl from "mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import {
  doc, getDoc,
} from "firebase/firestore";
import MAPBOX_KEY from "./config";

import { db } from "./firebase-config";

import "./Maps.css";

mapboxgl.accessToken = MAPBOX_KEY;

function Maps() {
  const { id } = useParams();
  const [center, setCenter] = useState(null);
  const [zoom, setZoom] = useState(null);
  const [geojson, setGeojson] = useState(null);
  const [mapInit, setMapInit] = useState(false);
  const mapContainer = useRef(null);

  useEffect(() => {
    if (center && zoom && geojson) return;
    const readFirestore = async () => {
      const docRef = doc(db, "maps", id);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        const {
          lat, lng, zoom: firestoreZoom, geojson: firestoreGeojson,
        } = docSnap.data();
        setZoom(firestoreZoom);
        setCenter([lng, lat]);
        setGeojson(firestoreGeojson);
      } else {
        console.error("No map found!");
      }
    };
    readFirestore();
  });

  useEffect(() => {
    if (!center || !zoom || !geojson) return;
    if (mapInit) return;
    setMapInit(true);
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center,
      zoom,
      attributionControl: false,
    }).addControl(new mapboxgl.AttributionControl({
      customAttribution: "<a href='https://ensembl.ai'>Powered by Ensembl</a>",
    }));

    map.on("load", async () => {
      map.addSource("locations", geojson);
      map.addLayer({
        id: "locations",
        type: "circle",
        source: "locations",
        paint: {
          "circle-color": "#4264fb",
          "circle-radius": 6,
          "circle-stroke-width": 2,
          "circle-stroke-color": "#ffffff",
        },
      });
    });

    const popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
    });
    map.on("mouseenter", "locations", (e) => {
      // Change the cursor style as a UI indicator
      map.getCanvas().style.cursor = "pointer";

      // Copy coordinates array.
      const coordinates = e.features[0].geometry.coordinates.slice();
      const { description } = e.features[0].properties;

      // Ensure that if the map is zoomed out such that multiple
      // copies of the feature are visible, the popup appears
      // over the copy being pointed to.
      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }
      // Populate the popup and set its coordinates
      // based on the feature found.
      popup.setLngLat(coordinates).setHTML(description).addTo(map);
    });
    map.on("mouseleave", "locations", () => {
      map.getCanvas().style.cursor = "";
      popup.remove();
    });
  }, [center, zoom, geojson]);

  return (
    <div>
      <div ref={mapContainer} id="map" />
    </div>
  );
}

export default Maps;
