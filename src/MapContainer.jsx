import mapboxgl from "mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import MapboxGeocoder from "@mapbox/mapbox-sdk/services/geocoding";

import PropTypes from "prop-types";

import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";

import {
  collection, addDoc, updateDoc, serverTimestamp, doc,
} from "firebase/firestore";
import React, { useRef, useEffect, useState } from "react";
import { db } from "./firebase-config";

import MAPBOX_KEY from "./config";

import "./MapContainer.css";
import "mapbox-gl/dist/mapbox-gl.css";

mapboxgl.accessToken = MAPBOX_KEY;

function MapContainer({ mapId, setMapId, locations }) {
  const mapContainer = useRef(null);

  const [lat, setLat] = useState(37);
  const [lng, setLng] = useState(-97);
  const [zoom, setZoom] = useState(3);

  const mapInit = useRef(false);

  const geocode = async () => {
    const features = {
      type: "geojson",
      data: {
        type: "FeatureCollection",
        features: [],
      },
    };
    const boundingBox = new mapboxgl.LngLatBounds();
    const geocoder = MapboxGeocoder({ accessToken: MAPBOX_KEY });
    await Promise.all((locations).map(async (obj) => {
      const res = await geocoder.forwardGeocode({
        query: obj.location,
        autocomplete: false,
        limit: 1,
      }).send();

      const coord = res.body.features[0].center;

      boundingBox.extend(coord);
      features.data.features.push({
        type: "Feature",
        properties: {
          description:
            `<div style="text-align:left;">
            <strong>
              ${obj.name || ""}
            </strong>
            <p>${obj.description || ""}</p></div>`,
        },
        geometry: {
          type: "Point",
          coordinates: coord,
        },
      });
    }));

    return { locations: features, boundingBox };
  };

  useEffect(() => {
    if (mapInit.current) return;
    mapInit.current = true;
    console.info("Init map...");
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom,
      attributionControl: false,
    }).addControl(new mapboxgl.AttributionControl({
      customAttribution: "<a href='https://ensembl.ai'>Powered by Ensembl</a>",
    }));

    const mapLocations = async () => {
      const { locations: geocodedLocations, boundingBox } = await geocode();

      map.on("load", async () => {
        map.addSource("locations", geocodedLocations);
        map.addLayer({
          id: "locations",
          type: "circle",
          source: "locations",
          paint: {
            "circle-color": "#5B85F2",
            "circle-radius": 5,
            "circle-stroke-width": 1,
            "circle-stroke-color": "#ffffff",
          },
        });
        map.fitBounds(boundingBox, {
          maxZoom: Object.keys(locations).length === 1 ? 9 : 15,
          padding: {
            top: 100, bottom: 100, left: 100, right: 100,
          },
        });
      });

      // Create a popup, but don't add it to the map yet.
      const popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false,
      });

      // map.fitBounds(boundingBox.toArray());
      map.on("mouseenter", "locations", (e) => {
        // Change the cursor style as a UI indicator
        map.getCanvas().style.cursor = "pointer";

        // Copy coordinates array.
        const coordinates = e.features[0].geometry.coordinates.slice();
        const { description } = e.features[0].properties;

        // Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
          coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }
        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.addClassName("map-popup")
          .setLngLat(coordinates)
          .setHTML(description)
          .addTo(map);
      });
      map.on("mouseleave", "locations", () => {
        map.getCanvas().style.cursor = "";
        popup.remove();
      });
      map.on("move", () => {
        setLng(map.getCenter().lng.toFixed(4));
        setLat(map.getCenter().lat.toFixed(4));
        setZoom(map.getZoom().toFixed(2));
      });

      const docRef = await addDoc(
        collection(db, "maps"),
        {
          createdAt: serverTimestamp(),
          updatedAt: serverTimestamp(),
        },
      );
      setMapId(docRef.id);
      await updateDoc(docRef, {
        geojson: geocodedLocations,
      });
    };

    mapLocations();
  });

  useEffect(() => {
    if (!mapInit.current) return () => { };
    const updateDb = async () => {
      const docRef = doc(db, "maps", mapId);
      await updateDoc(docRef, {
        updatedAt: serverTimestamp(),
        lng,
        lat,
        zoom,
      });
    };
    const timeoutId = setTimeout(updateDb, 500);
    return () => clearTimeout(timeoutId);
  }, [lng, lat, zoom]);

  return (
    <Stack direction="column" alignItems="center">
      <Box>
        <h3 style={{ display: "inline" }}>Step 2:</h3>
        <p style={{ marginLeft: "5px", display: "inline-block" }}>Move the map around, find the zoom level and the boundary you like. What you see below is what you will get!</p>
      </Box>
      <div ref={mapContainer} className="map-container" />
    </Stack>
  );
}

MapContainer.propTypes = {
  locations: PropTypes.arrayOf(PropTypes.any).isRequired, //eslint-disable-line
  mapId: PropTypes.string.isRequired,
  setMapId: PropTypes.func.isRequired,
};

export default MapContainer;
